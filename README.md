# Webex* Electron Linux

[Installation Instructions](#prebuilt-release)

# Why not Firefox or Chrome? / Why yet another Electron App?

I always had audio and video issues on Firefox and Chrome. Besides, the Webex extension is not working for me. But weirdly enough, the Webex Web version works flawlessly on Electron. 

# Is this a permanent solution?

No, it's meant to be a temporary solution until Cisco releases their client, as the Web version of Webex is still missing a lot of features.

# Does this temporary fix have any issues/bugs?

I haven't noticed anything.

# What works/doesn't work?

- You can't make polls

- Microphone and Camera work as expected.

- For Screen-sharing, [read below](#screen-sharing).

# Installation Guide

1. Install npm:

| Distribution | Installation |
| ------ | ------ |
| Ubuntu | `$ sudo apt install npm` |
| Fedora | `$ sudo dnf install npm` |
| openSUSE | `$ sudo zypper install npm` |
| Arch | `$ sudo pacman -S npm` |
| Other | <a href="https://github.com/npm/cli#installation" target="_blank">Official Command for Generic Linux</a> |

2. Using npm, install nativefier:
```bash
$ sudo npm install nativefier -g
```
3. Type the following command to make sure it's installed correctly:
```bash
$ nativefier -v
```
This should output the version number without any errors, like in this example:
```bash
$ nativefier -v
43.0.0
```
4. The template command is this:
```bash
$ nativefier -n "Webex-Teams" -p linux -a x64 --tray https://teams.webex.com
```
This command will create a folder inside the directory you executed the command. A good location for it would be `/opt`, but I personally selected `~/Documents`, since my `/home` is in a seperate partition, and I need to have access from multiple distros.

For convenience, in the above command, you might want to:
- change the name: `-n [name]`
- change the architecture: `-a [arch]` (x64 or arm64 for example)
- change the URL at the end with your provider's URL
- remove the `--tray` argument (potentially for Gnome, see below)

Run the command and wait for it to complete (at this point, it will cache the contents of the website. As a result, this might take a few minutes, depending on your network speed)

5. Test that it's working:
```bash
$ cd Webex-Teams-linux-x64 # Or the directory the previous command created
$ ./Webex-Teams # Or the name you put on the previous command
```
This should open a window. It might be blank for a few seconds, so wait for it to load completely. At this point, the Webex Teams login page should load. From there, login to your account, and proceed to use the Webex Web client as per usual.

A tray icon will appear as well, which might not appear on Gnome (in which case I suggest removing --tray from the command), or if it does (using a tray extension), left-clicking it will launch another instance (probably a bug), but right-clicking it and selecting it from the drop-down menu works properly. On plasma, the tray icon works properly overall. I haven't tested any other DE.

# Creating a Desktop Shortcut

Make a .desktop file to easily find it on the Menu:
```bash
$ sudo nano ~/.local/share/applications/webex-teams.desktop # Or use the editor of your choice
```
Then paste the following content:
```
[Desktop Entry]
Exec=[path]/Webex-Teams-linux-x64/Webex-Teams
Icon=[path]/Webex-Teams-linux-x64/resources/app/icon.png
Name=Webex Teams
Type=Application
```
Replace `[Path]` with the location of the folder created with `nativefier` (`/opt` or `/home/[your username]/Documents`, for example) or with the location you extracted the folder from the release.

**Don't forget to change the path accordingly, if you made any changes to the template command.**

Then open your Menu, find it and click on it. The very same window should appear. (Remember you can run more than 1 instances of this App, so if you didn't close the first one, then there will be 2 instances running, with 2 seperate tray icons)

If you see the app without the Webex Teams icon, then you need to download manually the icon from the internet, rename it to `icon.png` and place it on `[path]/Webex-Teams-linux-x64/resources/app`

# Screen-Sharing

For Screen-Sharing, on X11 it should work out of the box. As for wayland, it should also work with proper setup (xdg-desktop-portal, etc), though I didn't get the chance to do any testing on it.

# Congratulations! You've successfully finished the installation!

You're ready to experience a properly working Webex Web Client on Linux. Let other people know about this little trick!

# Sources

https://www.virtjunkie.com/webex-teams-on-linux/

https://github.com/tejzpr/webex-teams-linux

*Refers to Cisco WebEx®. All rights reserved to Cisco and/or its affiliates.

